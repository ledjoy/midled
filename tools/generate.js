sectorSize = 512; //������ ������� �� ������
tableElementSize = 8; //��� 32������ �����
firstDataSector = 3; //� ������ ���� ��������� � ������� ���
fileName = "img/midled.img"

var sizes = { //sizes of settings
	number: 4,
	string: 16,
}

var settings = {
	ledsPerFrame: 1,
	framesPerSecond: 25,
	bytesPerLed: 4,
	udpPort: 12345,
	channel: 0,
	ssid: "incendio",
	password: "3OdUdEtcolri",
};

var movies = [ 
	{note: 0, lengthInFrames: 1},
	{note: 1, lengthInFrames: 1},
	{note: 2, lengthInFrames: 1},
	{note: 3, lengthInFrames: 1},
	{note: 4, lengthInFrames: 1},
	{note: 5, lengthInFrames: 1},
	{note: 6, lengthInFrames: 1},
	{note: 7, lengthInFrames: 1},
	{note: 8, lengthInFrames: 1},
	{note: 9, lengthInFrames: 1},
	{note: 10, lengthInFrames: 1},
	{note: 11, lengthInFrames: 1},
	{note: 12, lengthInFrames: 1},
	{note: 13, lengthInFrames: 1},
	{note: 14, lengthInFrames: 1},
];

var buffers = { //����� ����� �������� ������ ��� ���������� � ����
	settings: Buffer.alloc(512),
	table: Buffer.alloc(1024),
	movies: [],
}

function rev(x) {
	var r = 0;
	for (var i=0;i<4;i++) {r<<=2;r|=(x&0x00000003);x>>=2;}
	return r;
}


//�������� ��������� � �����
settingsOffset = 0;
for(var key in settings) {
	switch (typeof settings[key]) {
	case "number":
		buffers.settings.writeUInt32LE(settings[key],settingsOffset);
		break;
	case "string":
		buffers.settings.write(settings[key],settingsOffset,sizes[typeof settings[key]]);
		break;		
	}
	settingsOffset+=sizes[typeof settings[key]];
}

//�������� ������ ��� ������
bytesPerFrame = settings.ledsPerFrame * settings.bytesPerLed;
movieOffset = firstDataSector;
for (i=0;i<movies.length;i++) {
	size = bytesPerFrame * movies[i].lengthInFrames;
	sectorsPerMovie = Math.ceil(size/sectorSize);
	buffers.movies[i]=Buffer.alloc(sectorsPerMovie * sectorSize);
	//��������� ������� �������
	tableElementOffset = tableElementSize * movies[i].note;
	buffers.table.writeUInt32LE(movieOffset,tableElementOffset);
	buffers.table.writeUInt32LE(movies[i].lengthInFrames,tableElementOffset+4);
//	console.log("movie #"+i+", allocated "+size+" bytes. First sector: "+movieOffset+", length: "+movies[i].lengthInFrames);
	movies[i].offset=movieOffset; //�������� ���� �� �������
	movieOffset += sectorsPerMovie;
}

buffers.movies[0].writeUInt32LE( 0x000000ff,0);
buffers.movies[1].writeUInt32LE( 0x0000ff00,0);
buffers.movies[2].writeUInt32LE( 0x0000ffff,0);
buffers.movies[3].writeUInt32LE( 0x00ff0000,0);
buffers.movies[4].writeUInt32LE( 0x00ff00ff,0);
buffers.movies[5].writeUInt32LE( 0x00ffff00,0);
buffers.movies[6].writeUInt32LE( 0x00ffffff,0);
buffers.movies[7].writeUInt32LE( 0xff000000,0);
buffers.movies[8].writeUInt32LE( 0xff0000ff,0);
buffers.movies[9].writeUInt32LE( 0xff00ff00,0);
buffers.movies[10].writeUInt32LE(0xff00ffff,0);
buffers.movies[11].writeUInt32LE(0xffff0000,0);
buffers.movies[12].writeUInt32LE(0xffff00ff,0);
buffers.movies[13].writeUInt32LE(0xffffff00,0);
buffers.movies[14].writeUInt32LE(0xffffffff,0);

//�������� ��������� �������� ������� �������� ������ ��� ������
/*
for (i=0;i<movies.length;i++) {
	colors = [
		{r:0,g:0,b:255},
		{r:0,g:255,b:0},
		{r:255,g:0,b:0},
		{r:0,g:255,b:255},
		{r:255,g:255,b:0},
		{r:255,g:0,b:255},
	];
	for(frame=0;frame<movies[i].lengthInFrames;frame++)
		for(led=0;led<settings.ledsPerFrame;led++) {
			offset = frame * bytesPerFrame + led * settings.bytesPerLed;
			r = Math.max(colors[i].r * frame / movies[i].lengthInFrames,colors[i+1].r * (movies[i].lengthInFrames - frame - 1) / movies[i].lengthInFrames);
			g = Math.max(colors[i].g * frame / movies[i].lengthInFrames,colors[i+1].g * (movies[i].lengthInFrames - frame - 1) / movies[i].lengthInFrames);
			b = Math.max(colors[i].b * frame / movies[i].lengthInFrames,colors[i+1].b * (movies[i].lengthInFrames - frame - 1) / movies[i].lengthInFrames);
			//r = rev(r);
			//g = rev(g);
			//b = rev(b);
			buffers.movies[i].writeUInt8(r,offset);
			buffers.movies[i].writeUInt8(g,offset+1);
			buffers.movies[i].writeUInt8(b,offset+2);
		}
}
*/

var fs = require('fs');
var wstream = fs.createWriteStream(fileName);
//��������� ���������
wstream.write(buffers.settings);
//��������� �������
wstream.write(buffers.table);
//��������� ������ �������
for (i=0;i<movies.length;i++) 
	wstream.write(buffers.movies[i]);
wstream.end();
